import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveProduct({product, isActive, fetchData}) {
	
	const [state, setState] = useState(isActive)

	const archiveToggle = (productId) => {
		fetch(`https://shrouded-scrubland-34485.herokuapp.com/products/${productId}/archive`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (data === true) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully archived'
				})
				setState(!state)
				fetchData()
			} else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'error',
					text: 'Please try again'
				})
				fetchData()
			}
		})
		setState(!state)
	}

	const reactivate = (productId) => {
		fetch(`https://shrouded-scrubland-34485.herokuapp.com/products/${productId}/restore`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if (data === true) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully restored'
				})
				
				fetchData()
			} else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'error',
					text: 'Please try again'
				})
				fetchData()
			}
		})

		setState(!state)
	}

	return(
		<>
			{state ?
				<Button variant="danger" size="sm" onClick={() => archiveToggle(product)}>Archive</Button>
				:

				<Button variant="success" size="sm" onClick={() => reactivate(product)}>Unarchive</Button>
			}


		</>



		)
}