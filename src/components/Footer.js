import React from "react";
import {
  Box,
  Container,
  Row,
  Column,
  FooterLink,
  Heading,
} from "./FooterStyles";
  
const Footer = () => {
  return (
    <Box>
     
      <Container>
        <Row>
          
          <Column>
            <FooterLink href="https://www.linkedin.com/in/jamesarrojo/">LinkedIn</FooterLink>
            
          </Column>
          
        </Row>
      </Container>
    </Box>
  );
};
export default Footer;