import React from 'react';
import { Card, Row, Col, Container } from 'react-bootstrap';
// import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';

export default function ProductCard({productProp}) {
	
	const { _id, name, description, price, image } = productProp;


	return(
        
                <Col xs={12} md={4} className="my-3">
            
                    <Card className="cardHighlight p-3 mt-3" >
                        <Card.Img variant="top" src={ image } />
                        <Card.Body>
                            <Card.Title>{ name }</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{ description }</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>Php { price }</Card.Text>

                            <Link className="btn btn-primary" to={`/products/${_id}`}>More Details</Link>
                            
                        </Card.Body>
                    </Card>
                   
                </Col>

		)
}

// // Check if the CourseCard component is getting the correct prop types
// // PropTypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next
// ProductCard.propTypes = {
// 	// shape() method it is used to check if a prop object conforms to a specific 'shape'
// 	productProp: PropTypes.shape({
// 		// Define the properties and their expected types
// 		name: PropTypes.string.isRequired,
// 		description: PropTypes.string.isRequired,
// 		price: PropTypes.number.isRequired
// 	})
// }