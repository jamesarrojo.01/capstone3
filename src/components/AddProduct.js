import React, { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function AddProduct({fetchData}) {
	
	// Add state for the fors of course
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [image, setImage] = useState('')

	// States for our modals to open/close
	const [showAdd, setShowAdd] = useState(false);

	// Functions to handle opening and closing of our AddCourse Modal
	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false)

	// Funtion for fetching the create course in the backend
	const addProduct = (e) => {
		e.preventDefault();

		fetch('https://shrouded-scrubland-34485.herokuapp.com/products/add', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('accessToken') }`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				image: image
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data) {
				// Show success message
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Product successfully added.'
				})
				// Close our modal
				closeAdd();
				// Render the updated data using the fetchData prop
				fetchData();
			} else {
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Product already exist!'
				})
				closeAdd();
			}

			// Clear out the input fields
			setName("")
			setDescription("")
			setPrice("")
		})
	}


	return(
		<>

			<Button variant="primary" onClick={openAdd}>Add New Product</Button>
		{/* Add Modal Forms */}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Image</Form.Label>
							<Form.Control type="text" value={image} onChange={e => setImage(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>

				</Form>
				

			</Modal>

		</>
		)
}