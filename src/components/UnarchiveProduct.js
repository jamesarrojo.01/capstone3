import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function UnarchiveProduct({product, isActive, fetchData}) {
	
	const reactivate = (productId) => {
		fetch(`https://shrouded-scrubland-34485.herokuapp.com/products/${productId}/restore`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})

		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (data === true) {
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'Product successfully disabled'
				})
				fetchData()
			} else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'error',
					text: 'Please try again'
				})
				fetchData()
			}
		})
	}


	return(
		<>
			{isActive ?
				<Button variant="danger" size="sm" >Archive</Button>
				:

				<Button variant="success" size="sm" onClick={() => reactivate(product)}>Unarchive</Button>
			}


		</>



		)
}