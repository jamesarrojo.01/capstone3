import React, { useContext, useState, useEffect } from 'react';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';
import { Card, Row, Col, Container } from 'react-bootstrap';
import UserContext from '../UserContext';

export default function Products() {
	
	const { user } = useContext(UserContext);

	const[allProducts, setAllProducts] = useState([]);

	const fetchData = () => {
		fetch('https://shrouded-scrubland-34485.herokuapp.com/products/all')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setAllProducts(data)
		})
	}
	
	useEffect(() => {
		fetchData()
	}, [])

	return(
		<>
			{(user.isAdmin === true) ?
				<AdminView productsData={allProducts} fetchData={fetchData}/>

				:
<Container className="d-flex">
            <Row className='pt-3 m-auto align-self-center'>
				<UserView productsData={allProducts}/>
                </Row>
        </Container>
			}



		</>

		

		)
}