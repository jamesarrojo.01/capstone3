import React from 'react';
import { Carousel } from 'react-bootstrap'


export default function Home() {
	return(
		<>
			<Carousel variant="light">
				<Carousel.Item>
					<img
					className="d-block w-100"
					src="https://im.uniqlo.com/global-cms/spa/res24d3e08e2d5bf8b1be8a4c6f6f85c406fr.jpg"
					alt="First slide"
					/>
					<Carousel.Caption>
					<h5>Comfortable fitting loose shorts.</h5>
					<p>Women Chino Shorts</p>
					</Carousel.Caption>
				</Carousel.Item>
				<Carousel.Item>
					<img
					className="d-block w-100"
					src="https://im.uniqlo.com/global-cms/spa/res7edd354abadd613d6658e54a66e85806fr.jpg"
					alt="Second slide"
					/>
					<Carousel.Caption>
					<h5>Effortlessly chic in breezy shorts.</h5>
					<p>Women Linen Cotton Shorts</p>
					</Carousel.Caption>
				</Carousel.Item>
				<Carousel.Item>
					<img
					className="d-block w-100"
					src="https://im.uniqlo.com/global-cms/spa/res6c6683d2b61d48fd80b7b28a2d05f1bafr.jpg"
					alt="Third slide"
					/>
					<Carousel.Caption>
					<h5>Versatile shorts on land or at sea.</h5>
					<p>Swim Active Shorts</p>
					</Carousel.Caption>
				</Carousel.Item>
				<Carousel.Item>
					<img
					className="d-block w-100"
					src="https://im.uniqlo.com/global-cms/spa/res34a0c9099b607c9a7d772a4a7c8d6d8ffr.jpg"
					alt="Third slide"
					/>
					<Carousel.Caption>
					<h5>A small twist for toughness.</h5>
					<p>Chino Shorts</p>
					</Carousel.Caption>
				</Carousel.Item>
			</Carousel>


		</>



		)
}