import React, { useState, useEffect, useContext } from 'react';
import { Container, Form, Button, Figure, Row } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function SpecificProduct() {

	const navigate = useNavigate();

	//useParams() contains any values we are tryilng to pass in the URL stored
	//useParams is how we receive the courseId passed via the URL
	const { productId } = useParams();

	useEffect(() => {

		fetch(`https://shrouded-scrubland-34485.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
            setImage(data.image)
		})

	} ,[])

	const { user } = useContext(UserContext);

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
    const [image, setImage] = useState('')

	useEffect(() => {
		
	})

	const [qty, setQty] = useState(0)

// add quantity
	const addOne = () => {
		
		setQty(qty + 1)
		
	}

	const subOne = () => {
		if (qty > 0) {
			setQty(qty - 1)
			
		} else {
			setQty(0)
		}
	}


//addToCart function --> WARNING: NOT YET DONE!!! will crash the server if attached to "Add to Cart" button
	const addToCart = () => {
		console.log(productId)
		fetch('https://shrouded-scrubland-34485.herokuapp.com/orders/addToCart', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: qty,
				subTotal: qty * price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data) {
				Swal.fire({
					title: 'Successfully enrolled',
					icon: 'success'
				})

				navigate('/products')
			}else {
				Swal.fire({
					title:'Something went wrong',
					icon: 'error'

				})
			}
		})
	}


	return (
		<Container>
            <Row>
			{/* <Card style={{ width: '50rem' }}>
				<Card.Header>
					<h4>{name}</h4>
				</Card.Header>
                <Card.Img variant="top" src={ image } />
				<Card.Body>
					<Card.Text>{description}</Card.Text>
					<h6>Price: Php {price}</h6>
				</Card.Body>

				<Card.Footer>

					{
                        user.accessToken !== null ?
                        <div className="d-grid gap-2">
                            <Button variant="primary" onClick={() => enroll(productId)}>
                                Add to Cart
                            </Button>
                        </div>
                        :
                        <Link className="btn btn-warning d-grid gap-2" to="/login">
                            Login to Enroll
                        </Link>
                    }
					
				</Card.Footer>
			</Card> */}
            <Figure className='col-md-6'>
                <Figure.Image
                    width={500}
                    src={ image }
                />
                
            </Figure>
            <section className='col-md-6'>
                <h1>{ name }</h1>
                <p className='mt-5'>{ description }</p>
                <p className='mt-5'>Php { price }</p>
				<Form inline className='d-flex col-3 mb-5'>
					<Button onClick={subOne}>-</Button><Form.Control type="number" placeholder="0" value={qty} onChange={e => setQty(Number(e.target.value))} /> <Button onClick={addOne}>+</Button>
					
				</Form>
				<p>Subtotal: Php { price * qty }</p>
				<Button className='mt-5'>Add to Cart</Button>
            </section>
			
            </Row>
		</Container>

		)
}


