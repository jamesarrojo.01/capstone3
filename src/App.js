import React, { useState } from 'react'
import './App.css';
import AppNavbar from './components/AppNavbar'

import Home from './pages/Home'
import Products from './pages/Products'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'

import { UserProvider } from './UserContext';

import { Container } from 'react-bootstrap'


import { BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import SpecificProduct from './pages/SpecificProduct';


function App() {
  
  const [user, setUser] = useState({
    accessToken: localStorage.getItem('accessToken'),
    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  })

  const unsetUser = () => {
    localStorage.clear()
  }



  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Routes>
            < Route path="/" element={ <Home /> } />
            < Route path="/products" element={ <Products /> } />
            < Route path="/register" element={ <Register /> } />
            < Route path="/login" element={ <Login /> } />
            < Route path="/logout" element={ <Logout /> } />
            < Route path="/products/:productId" element={ <SpecificProduct /> } />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
